import QtQuick 2.2
import QtQuick.Controls 1.1

ApplicationWindow {
    visible: true
    width: 128
    height: 128
    title: qsTr("Hello World")

    Text {
        id: volume_number
        text: qsTr("Hello World")
        anchors.centerIn: parent
    }
}
