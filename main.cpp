#include <QApplication>
#include <QQmlApplicationEngine>
#include<QCommandLineParser>
#include<QCommandLineOption>
#include<QProcess>
#include<QDebug>
const QString DEVICE="Master";
struct VolumeInfo{
    int min;
    int max;
    int current;
    bool mute;
};
struct VolumeInfo getVolume();
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("operation",QCoreApplication::translate("main","对主音量进行的操作"));
    parser.addPositionalArgument("value",QCoreApplication::translate("main","传给该操作的数值"));
    parser.process(app);

    const QStringList args=parser.positionalArguments();
    if(args[0].compare("up",Qt::CaseInsensitive)==0){
        VolumeInfo info=getVolume();
        qDebug()<<info.max<<","<<info.min<<","<<info.current;
    }

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}

struct VolumeInfo getVolume(){
    QProcess process;
    process.start("amixer get "+DEVICE);
    process.waitForFinished();
    process.readLine();
    process.readLine();
    process.readLine();
    VolumeInfo info;
    QString range=((QString)(process.readLine())).split("Playback")[1];
    info.min=range.split('-')[0].toInt();
    info.max=range.split('-')[1].toInt();
    QStringList status=((QString)(process.readLine())).split("Playback")[1].split(' ',QString::SkipEmptyParts);
    info.current=status[0].toInt();
    info.mute=status[3].contains('f');
    qDebug()<<info.mute;
    return info;
}
